<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Webnotifications;
use App\Events;
use Illuminate\Support\Facades\DB;
use App\Gallery;

class userController extends Controller
{

    public function index()
    {

        return view('welcome');
        // return "aa";
          $move = DB::table('webnotifications')
        ->latest()
        ->take(1)
        ->get();


         $webnoti= DB::table('webnotifications')
        ->latest()
        ->take(4)
        ->get();
        $events= DB::table('events')
        ->latest()
        ->take(4)
        ->get();



        $service= DB::table('courses')
        ->latest()
        ->take(2)
        ->get();

        $team= DB::table('teams')
        ->latest()
        ->take(3)
        ->get();


        $events= DB::table('events')
        ->latest()
        ->take(3)
        ->get();
        $webnoti= DB::table('Webnotifications')
        ->latest()
        ->take(1)
        ->get();


        $says=DB::select('select * from says order by id DESC');
        // $events=DB::select('select * from events order by id DESC');
        $count = Events::count();
        // $webnoti=DB::select('select * from webnotifications order by id DESC');
        $webcount = Webnotifications::count();
        // ,compact('move','says','course','events','count','webnoti','webcount')
          return view('hospital.index',compact('service','team','events','says','webnoti'));

    }


    public function gallery()
    {
        $gallery=DB::select('select * from galleries order by id DESC');
        $count = Gallery::count();
        return view('school.gallery',compact('gallery','count'));
    }

    public function contact()
    {

        return view('hospital.contact');
    }

    public function about()
    {

        return view('hospital.about');
    }
    public function apo()
    {

        return view('hospital.apo');
    }

    public function doctors()
    {
        $team=DB::select('select * from teams order by id DESC');
        return view('hospital.doctors',compact('team'));
    }

    public function galleryuser()
    {
        $pic=DB::select('select * from galleries order by id DESC');
        return view('hospital.gallery',compact('pic'));
    }

    public function userservice()
    {
        $service=DB::select('select * from courses order by id DESC');
        return view('hospital.service',compact('service'));
    }

    public function uservideos()
    {
        $video=DB::select('select * from videos order by id DESC');
        return view('hospital.videos',compact('video'));
    }


}
