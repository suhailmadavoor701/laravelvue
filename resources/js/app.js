require('./bootstrap');
   
window.Vue = require('vue');
import VueRouter from 'vue-router'
Vue.use(VueRouter)
   

const routes = [
  { path: '/foo', component: require('./components/ExampleComponent.vue').default },
  { path: '/bar', component: require('./components/User.vue').default },
  { path: '/about', component: require('./components/About.vue').default },
  { path: '/home', component: require('./components/Index.vue').default },
  { path: '/', component: require('./components/Index.vue').default }
]
  
const router = new VueRouter({
  routes 
})
  
const app = new Vue({
  router
}).$mount('#app')

