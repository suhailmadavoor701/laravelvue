@extends('layouts.users')
@section('content')

    <div id="content" class="content">
        <!--Banner Inner-->
        <section>
            <div class="lgx-banner lgx-banner-inner">
                <div class="lgx-page-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="lgx-heading-area">
                                    <div class="lgx-heading lgx-heading-white">
                                        <h2 class="heading">Get In Touch</h2>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li><a href="/"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
                                        <li class="active">Contact</li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--//.ROW-->
                    </div><!-- //.CONTAINER -->
                </div><!-- //.INNER -->
            </div>
        </section> <!--//.Banner Inner-->


        <main>
            <div class="lgx-page-wrapper">
                <!--News-->
                <section>


                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="lgx-contact-form">


                                    <form action="/enquiry" method="post">
                                        @csrf
                                        @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    @if(Session::has('status'))
                                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('status') }}</p>
                                    @endif


                                    <div class="lgx-contact-inner">
                                        <div class="lgx-contact-form-inline">
                                            <span class="wpcf7-form-control-wrap your-name">
                                                <input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Your Name ..."></span><br>
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Your Email ..."></span><br>
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <input type="number" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Your Phone Number ..."></span><br>
                                            <span class="wpcf7-form-control-wrap your-subject">
                                                <input type="text" name="subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Subject Here ..."></span>
                                        </div>
                                        <p>   <span class="wpcf7-form-control-wrap your-message"><textarea name="message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Write some text ..."></textarea></span>
                                        </p></div>
                                {{-- <p><span class="lgx-btn">
                                    <input type="submit" value="Send Message" class="wpcf7-form-control wpcf7-submit">
                                </span></p></div> --}}
                                <div class="form-group col-lg-12 col-md-12 col-sm-12 text-center">
                                    <button type="submit" class="readon register-btn"><span class="txt">Register</span></button>
                                </div>

                                </form>
                                </div>
                            </div>
                        </div>
                        <!--//.ROW-->
                    </div><!-- //.CONTAINER -->
                </section>
                <!--News END-->
            </div>

    {{-- <div  style="border-style: none!important" class="contact_map mt-100">
        <div style="padding-right: 10px;padding-left: 10px;" class="map-area">
           <div  id="googleMap">



            <iframe   src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d125744.3525882653!2d76.31667200000001!3d9.974579199999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba64b7c540e5e05%3A0xc2b6fa34fd3f4553!2sStecOn%20Polymers%20Pvt%20Ltd%2C%20MLA%20Rd%2C%20KALLENGAL%20PADI%2C%20Oorakam%2C%20Kerala%20676519!5e0!3m2!1sen!2sin!4v1629439762174!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

               </div>
        </div>
     </div> --}}
        </main>
    </div>
@endsection
