<!doctype html>
<html class="no-js" lang="en">

<!-- Mirrored from httpcoder.com/demo/html/medicalo/view/appoinment.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 09 Sep 2021 10:45:09 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <!-- The above 3 meta tags *must* come first in the head -->

    <!-- SITE TITLE -->
    <title>Medicalo</title>
    <meta name="description" content="Responsive Medical HTML Template"/>
    <meta name="keywords" content="Bootstrap3, Medical,  Doctor, Hospital, Template, Responsive, HTML5"/>
    <meta name="author" content="httpcoder.com"/>

    <!-- twitter card starts from here, if you don't need remove this section -->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@yourtwitterusername"/>
    <meta name="twitter:creator" content="@yourtwitterusername"/>
    <meta name="twitter:url" content="http://yourdomain.com/"/>
    <meta name="twitter:title" content="Your home page title, max 140 char"/>
    <!-- maximum 140 char -->
    <meta name="twitter:description" content="Your site description, maximum 140 char "/>
    <!-- maximum 140 char -->
    <meta name="twitter:image" content="assets/img/twittercardimg/twittercard-280-150.jpg"/>
    <!-- when you post this page url in twitter , this image will be shown -->
    <!-- twitter card ends from here -->

    <!-- facebook open graph starts from here, if you don't need then delete open graph related  -->
    <meta property="og:title" content="Your home page title"/>
    <meta property="og:url" content="http://your domain here.com"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:site_name" content="Your site name here"/>
    <!--meta property="fb:admins" content="" /-->  <!-- use this if you have  -->
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="assets/img/opengraph/fbphoto.jpg"/>
    <!-- when you post this page url in facebook , this image will be shown -->
    <!-- facebook open graph ends from here -->

    <!--  FAVICON AND TOUCH ICONS -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico"/>
    <!-- this icon shows in browser toolbar -->
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico"/>
    <!-- this icon shows in browser toolbar -->
    <link rel="apple-touch-icon" sizes="57x57" href="assets/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="assets/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="assets/img/favicon/manifest.json">

    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="assets/libs/bootstrap/css/bootstrap.min.css" media="all"/>

    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="assets/libs/fontawesome/css/font-awesome.min.css" media="all"/>

    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="assets/libs/maginificpopup/magnific-popup.css" media="all"/>

    <!-- OWL CAROUSEL CSS -->
    <link rel="stylesheet" href="assets/libs/owlcarousel/owl.carousel.min.css" media="all" />
    <link rel="stylesheet" href="assets/libs/owlcarousel/owl.theme.default.min.css" media="all" />

    <!-- GOOGLE FONT -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,600,600i,700,800,900"/>

    <link rel="stylesheet" href="assets/libs/datepicker/bootstrap-datetimepicker.min.css" media="all"/>

    <!-- MASTER  STYLESHEET  -->
    <link id="lgx-master-style" rel="stylesheet" href="assets/css/style-default.min.css" media="all"/>

    <!-- MODERNIZER CSS  -->
    <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body class="home">

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div class="lgx-container ">
    <!-- ***  ADD YOUR SITE CONTENT HERE *** -->


    <!--HEADER-->
    <header>
        <div id="lgx-header" class="lgx-header">
            <div class="lgx-header-position lgx-header-position-white lgx-header-position-fixed "> <!--lgx-header-position-fixed lgx-header-position-white lgx-header-fixed-container lgx-header-fixed-container-gap lgx-header-position-white-->
                <div class="lgx-container"> <!--lgx-container-fluid-->
                    <nav class="navbar navbar-default lgx-navbar">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div class="lgx-logo">
                                <a href="index.html" class="lgx-scroll">
                                    <img src="assets/img/logo-white2.png" alt="Logo"/>
                                </a>
                            </div>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <div class="lgx-nav-right navbar-right">
                                <div class="lgx-cart-area">
                                    <a class="lgx-btn lgx-btn-red" href="appoinment.html"><span>Appoinment</span></a>
                                </div>
                            </div>
                            <ul class="nav navbar-nav lgx-nav navbar-right">
                                <li>
                                    <a href="index.html" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home <span class="caret"></span></a>
                                    <ul class="dropdown-menu multi-level">
                                        <li><a href="index.html">Home (Default)</a></li>
                                        <li><a href="index2.html">Home Banner</a></li>
                                        <li><a href="index3.html">Home Banner Two</a></li>
                                        <li><a href="index4.html">Home Typed</a></li>
                                        <li><a href="index-slider.html">Home Slider</a></li>
                                    </ul>
                                <li>
                                <li>
                                    <a href="index.html" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages <span class="caret"></span></a>
                                    <ul class="dropdown-menu multi-level">
                                        <li><a href="about.html">About</a></li>
                                        <li class="dropdown-submenu">
                                            <a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Doctors <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="doctors.html">Doctors List</a></li>
                                                <li><a href="doctor.html">Doctors Single</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="services.html">Services List</a></li>
                                        <li><a href="appoinment.html">Appoinment</a></li>
                                        <li class="dropdown-submenu">
                                            <a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">News <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="news.html">News</a></li>
                                                <li><a href="news-single.html">News Single</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="contact.html">Contact</a></li>
                                        <li class="dropdown-submenu">
                                            <a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-submenu">
                                                    <a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown Two<span class="caret"></span></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#">Dropdown Three</a></li>
                                                        <li><a href="#">Dropdown Three</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="#">Dropdown Two</a></li>
                                                <li><a href="#">Dropdown Two</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="lgx-scroll" href="doctors.html">Doctors</a></li>
                                <li><a class="lgx-scroll" href="services.html">Services</a></li>
                                <li><a class="lgx-scroll" href="news.html">News</a></li>
                                <li><a class="lgx-scroll" href="contact.html">Contact</a></li>
                            </ul>
                        </div><!--/.nav-collapse -->
                    </nav>
                </div>
                <!-- //.CONTAINER -->
            </div>
        </div>
    </header>
    <!--HEADER END-->

    <div id="content" class="content">
        <!--Banner Inner-->
        <section>
            <div class="lgx-banner lgx-banner-inner">
                <div class="lgx-page-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="lgx-heading-area">
                                    <div class="lgx-heading lgx-heading-white">
                                        <h2 class="heading">Appoinment</h2>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li><a href="index.html"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
                                        <li class="active">Appoinment</li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--//.ROW-->
                    </div><!-- //.CONTAINER -->
                </div><!-- //.INNER -->
            </div>
        </section> <!--//.Banner Inner-->


        <main>
            <div class="lgx-page-wrapper">
                <!--News-->
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div id="lgx-appoinment" class="lgx-appoinment">
                                    <div class="lgx-heading lgx-heading-xs lgx-heading-gap-xs">  <!--lgx-heading-white-->
                                        <h2 class="heading">Make An <span>Appoinment</span></h2>
                                        <p class="subheading">Just make an appointment to get help from our experts</p>
                                    </div>
                                    <div class="lgx-appoinment-form-area">
                                        <div class="lgx-appoinment-form">
                                            <select name="your-browser" class="wpcf7-form-control wpcf7-select lgx-select lgx-form-inline">
                                                <option value="Personal - $39">Select Depertment ...</option>
                                                <option value="Business - $89">Depertment One</option>
                                                <option value="Premium - $189">Depertment Two</option>
                                                <option value="Premium - $189">Depertment Three</option>
                                            </select>
                                            <select name="your-browser" class="wpcf7-form-control wpcf7-select lgx-select lgx-form-inline">
                                                <option value="Personal - $39">Select Doctor ...</option>
                                                <option value="Business - $89">Dr. Jonathon</option>
                                                <option value="Premium - $189">Dr. Devid</option>
                                                <option value="Premium - $189">Dr. Elina</option>
                                            </select>
                                            <div class="input-append date form_datetime lgx-form_datetime lgx-form-inline">
                                                <input  type="text" class="form-control wpcf7-form-control" placeholder="Date & Time ..." value="" readonly>
                                                <span class="add-on"><i class="icon-th"><span class="glyphicon glyphicon-calendar"></span></i></span>
                                            </div>
                                            <input name="text" value="" class="wpcf7-form-control form-control lgx-form-inline" placeholder="Your Name ..." type="text">
                                            <input name="email" value="" class="wpcf7-form-control form-control lgx-form-inline" placeholder="Your E-mail ..." type="email">
                                            <!--<input value="Registration Now" class="wpcf7-form-control wpcf7-submit lgx-form-inline lgx-submit lgx-btn" type="submit">-->
                                            <a class="wpcf7-form-control wpcf7-submit lgx-form-inline lgx-submit lgx-btn" >Registration Now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--//.ROW-->
                    </div><!-- //.CONTAINER -->
                </section>
                <!--News END-->
            </div>
        </main>
    </div>

    <!--FOOTER-->
    <footer>
        <!--Fixed the footer for class "lgx-footer-fixed"-->
        <div id="lgx-footer" class="lgx-footer lgx-footer-fixed"> <!--lgx-footer-black-->
            <div class="lgx-inner-footer">
                <div class="container">
                    <div class="lgx-footer-area">
                        <div class="lgx-footer-single">
                            <a class="logo" href="index.html"><img src="assets/img/logo.png" alt="Logo"></a>
                            <h3 class="footer-contact">
                                <a href="http://httpcoder.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="dfb6b1b9b09fb2babbb6bcbeb3b0f1bcb0b2">[email&#160;protected]</a> <br>
                                +880 02 356 896
                            </h3>
                        </div> <!--//footer-area-->
                        <div class="lgx-footer-single">
                            <h3 class="footer-title">Quick Links</h3>
                            <ul class="list-unstyled lgx-footer-link">
                                <li><a href="#">How It Works</a></li>
                                <li><a href="#">Guarantee</a></li>
                                <li><a href="#">Report Bug</a></li>
                                <li><a href="#">Pricing</a></li>
                            </ul>
                        </div>
                        <div class="lgx-footer-single">
                            <h3 class="footer-title">Our Services</h3>
                            <ul class="list-unstyled lgx-footer-link">
                                <li><a href="#">Choosing Doctor</a></li>
                                <li><a href="#">proper Treatment</a></li>
                                <li><a href="#">Discuss Doctor</a></li>
                                <li><a href="#">Medical Care</a></li>
                            </ul>
                        </div>
                        <div class="lgx-footer-single">
                            <h3 class="footer-title">Departments</h3>
                            <ul class="list-unstyled lgx-footer-link">
                                <li><a href="#">Dental Surgery</a></li>
                                <li><a href="#">Family Health</a></li>
                                <li><a href="#">Report Bug</a></li>
                                <li><a href="#">Eye Care</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="lgx-footer-bottom">
                        <div class="lgx-copyright">
                            <p>Copyright <span>©</span> 2018 All Rights Reserved By <a href="#">Medicalo</a></p>
                        </div>
                    </div>

                </div>
                <!-- //.CONTAINER -->
            </div>
            <!-- //.footer Middle -->
        </div>
    </footer>
    <!--FOOTER END-->
    <a class="lgx-backtop show"></a>

</div>
<!--//.LGX SITE CONTAINER-->
<!-- *** ADD YOUR SITE SCRIPT HERE *** -->
<!-- JQUERY  -->

<script data-cfasync="false" src="../../../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="assets/js/vendor/jquery-1.12.4.min.js"></script>

<!-- BOOTSTRAP JS  -->
<script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/libs/datepicker/bootstrap-datetimepicker.js"></script>

<!-- Smooth Scroll  -->
<script src="assets/libs/jquery.smooth-scroll.js"></script>

<!-- SKILLS SCRIPT  -->
<script src="assets/libs/jquery.validate.js"></script>

<!-- adding magnific popup js library -->
<script type="text/javascript" src="assets/libs/maginificpopup/jquery.magnific-popup.min.js"></script>

<!-- Owl Carousel  -->
<script src="assets/libs/owlcarousel/owl.carousel.min.js"></script>

<!-- Counter JS -->
<script src="assets/libs/waypoints.min.js"></script>
<script src="assets/libs/counterup/jquery.counterup.min.js"></script>

<!-- SMOTH SCROLL -->
<script src="assets/libs/jquery.smooth-scroll.min.js"></script>
<script src="assets/libs/jquery.easing.min.js"></script>

<!-- type js -->
<script src="assets/libs/typed/typed.min.js"></script>

<!-- header parallax js -->
<script src="assets/libs/header-parallax.js"></script>

<!-- instafeed js -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/instafeed.js/1.4.1/instafeed.min.js"></script>-->
<script src="assets/libs/instafeed.min.js"></script>

<!-- CUSTOM SCRIPT  -->
<script src="assets/js/custom.script.js"></script>

<div class="lgx-switcher-loader"></div>
<!-- For Demo Purpose Only// Remove From Live -->
<script src="switcher/js/switcherd41d.js?"></script>
<!-- For Demo Purpose Only //Remove From Live-->

</body>

<!-- Mirrored from httpcoder.com/demo/html/medicalo/view/appoinment.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 09 Sep 2021 10:45:11 GMT -->
</html>
