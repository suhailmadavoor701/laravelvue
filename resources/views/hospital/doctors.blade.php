@extends('layouts.users')
@section('content')
    <div id="content" class="content">
        <!--Banner Inner-->
        <section>
            <div class="lgx-banner lgx-banner-inner">
                <div class="lgx-page-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="lgx-heading-area">
                                    <div class="lgx-heading lgx-heading-white">
                                        <h2 class="heading">Team Of Consultants</h2>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li><a href="/"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
                                        <li class="active">Team Of Consultants</li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--//.ROW-->
                    </div><!-- //.CONTAINER -->
                </div><!-- //.INNER -->
            </div>
        </section> <!--//.Banner Inner-->


        <main>
            <div class="lgx-page-wrapper">
                <!--News-->
                <section>
                    <div  class="container">
                        <div  class="row">



                             @foreach ($team as $item)
                             <div  class="col-xs-12 col-sm-6 col-md-4">
                                <div  class="lgx-single-team"> <!---->
                                    <figure>
                                        <a class="profile-img" href="/doctors"><img src="uploads/team/{{$item->image}}" alt="speaker"/></a>
                                        <figcaption>

                                            <div class="team-info">
                                                <h3 class="title"><a href="/doctors">{{$item->name}}</a></h3>
                                                <h4 class="subtitle">{{$item->department}}</h4>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>

                             @endforeach







                        </div>
                        <!--//.ROW-->
                    </div><!-- //.CONTAINER -->
                </section>
                <!--News END-->
            </div>
        </main>
    </div>
@endsection
