@extends('layouts.users')
@section('content')


<div id="content" class="content">
    <!--Banner Inner-->
    <section>
        <div class="lgx-banner lgx-banner-inner">
            <div class="lgx-page-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-heading-area">
                                <div class="lgx-heading lgx-heading-white">
                                    <h2 class="heading">Gallery</h2>
                                </div>
                                <ul class="breadcrumb">
                                    <li><a href="/"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
                                    <li class="active">Gallery</li>
                                </ul>
                            </div>
                        </div>
                    </div><!--//.ROW-->
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section> <!--//.Banner Inner-->


    <main>
        <div class="lgx-page-wrapper">
            <!--News-->
            <section>
                <div class="container">
                    <div class="row">

                    @foreach ($pic as $item)
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="lgx-single-news">
                            <figure>
                                <a href="/"><img src="uploads/gallery/{{$item->image}}" alt=""></a>

                            </figure>
                            <div class="single-news-info">

                                <h3 class="title"><a href="">{{$item->name}}</a></h3>

                            </div>
                        </div>
                    </div>
                    @endforeach
                    </div>
                </div><!-- //.CONTAINER -->
            </section>
            <!--News END-->
        </div>
    </main>
</div>

@endsection
