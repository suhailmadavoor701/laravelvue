@extends('layouts.users')
@section('content')



<div id="content" class="content">
    <!--Banner Inner-->
    <section>
        <div class="lgx-banner lgx-banner-inner">
            <div class="lgx-page-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-heading-area">
                                <div class="lgx-heading lgx-heading-white">
                                    <h2 class="heading">Our Best Departments</h2>
                                </div>
                                <ul class="breadcrumb">
                                    <li><a href="/"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
                                    <li class="active">Our Best Departments</li>
                                </ul>
                            </div>
                        </div>
                    </div><!--//.ROW-->
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section> <!--//.Banner Inner-->


    <main>
        <div class="lgx-page-wrapper">
            <!--News-->
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-service-area lgx-service-area-similar">


                               @foreach ($service as $item)

                               <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="lgx-single-news">
                                    <figure>
                                        <a href=""><img src="/uploads/course/{{$item->image}}" alt=""></a>
                                        {{-- <h3 class="date">02 Sep, 2019</h3> --}}
                                    </figure>
                                    <div class="single-news-info">
                                        {{-- <h3 class="cat"><a href="#">Health</a></h3> --}}
                                        <h3 class="title"><a href="">{{$item->name}}</a></h3>
                                        <p>{{$item->description}}</p>
                                        {{-- <a class="lgx-btn lgx-btn-white lgx-btn-sm" href="#"><span>Read More</span></a> --}}
                                    </div>
                                </div>
                            </div>

                               @endforeach



                            </div>
                        </div>
                    </div>
                    <!--//.ROW-->
                </div><!-- //.CONTAINER -->
            </section>
            <!--News END-->
        </div>
    </main>
</div>
@endsection
