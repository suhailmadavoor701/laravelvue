@extends('layouts.users')
@section('content')
<div id="content" class="content">
    <!--Banner Inner-->
    <section>
        <div class="lgx-banner lgx-banner-inner">
            <div class="lgx-page-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-heading-area">
                                <div class="lgx-heading lgx-heading-white">
                                    <h2 class="heading">Our Awarness Videos</h2>
                                </div>
                                <ul class="breadcrumb">
                                    <li><a href="/"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
                                    <li class="active">Our Awarness Videos</li>
                                </ul>
                            </div>
                        </div>
                    </div><!--//.ROW-->
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section> <!--//.Banner Inner-->


    <main>
        <div class="lgx-page-wrapper">
            <!--News-->
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-service-area lgx-service-area-similar">


                            @foreach ($video as $item)
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div style="justify-content: center"  style="width:100%" class="lgx-single-news">
                                    <figure>
                                        <iframe style="justify-content: center" width="100%;" height="200px;" src="https://www.youtube.com/embed/{{$item->link}}"
                                        title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;
                                        clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


                                    </figure>
                                    <div class="single-news-info">

                                        <h3 class="title"><a href="">{{$item->title}}</a></h3>

                                    </div>
                                </div>
                            </div>

                            @endforeach










                            </div>
                        </div>
                    </div>
                    <!--//.ROW-->
                </div><!-- //.CONTAINER -->
            </section>
            <!--News END-->
        </div>
    </main>
</div>
@endsection
