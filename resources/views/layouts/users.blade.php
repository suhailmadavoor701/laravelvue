<!doctype html>
<html class="no-js" lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <!-- The above 3 meta tags *must* come first in the head -->

    <!-- SITE TITLE -->
    <title>Medicalo</title>
    <meta name="description" content="Responsive Medical HTML Template"/>
    <meta name="keywords" content="Bootstrap3, Medical,  Doctor, Hospital, Template, Responsive, HTML5"/>
    <meta name="author" content="httpcoder.com"/>

    <!-- twitter card starts from here, if you don't need remove this section -->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@yourtwitterusername"/>
    <meta name="twitter:creator" content="@yourtwitterusername"/>
    <meta name="twitter:url" content="http://yourdomain.com/"/>
    <meta name="twitter:title" content="Your home page title, max 140 char"/>
    <!-- maximum 140 char -->
    <meta name="twitter:description" content="Your site description, maximum 140 char "/>
    <!-- maximum 140 char -->
    <meta name="twitter:image" content="assets/img/twittercardimg/twittercard-280-150.jpg"/>
    <!-- when you post this page url in twitter , this image will be shown -->
    <!-- twitter card ends from here -->

    <!-- facebook open graph starts from here, if you don't need then delete open graph related  -->
    <meta property="og:title" content="Your home page title"/>
    <meta property="og:url" content="http://your domain here.com"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:site_name" content="Your site name here"/>
    <!--meta property="fb:admins" content="" /-->  <!-- use this if you have  -->
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="assets/img/opengraph/fbphoto.jpg"/>
    <!-- when you post this page url in facebook , this image will be shown -->
    <!-- facebook open graph ends from here -->

    <!--  FAVICON AND TOUCH ICONS -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico"/>
    <!-- this icon shows in browser toolbar -->
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico"/>
    <!-- this icon shows in browser toolbar -->
    <link rel="apple-touch-icon" sizes="57x57" href="assets/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="assets/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon/favicon-16x16.png">

    <link rel="manifest" href="{{url('users/assets/img/favicon/manifest.json')}}">
    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="{{url('users/assets/libs/bootstrap/css/bootstrap.min.css')}}" media="all"/>

    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="{{url('users/assets/libs/fontawesome/css/font-awesome.min.css')}}" media="all"/>

    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="{{url('users/assets/libs/maginificpopup/magnific-popup.css')}}" media="all"/>

    <!-- OWL CAROUSEL CSS -->
    <link rel="stylesheet" href="{{url('users/assets/libs/owlcarousel/owl.carousel.min.css')}}" media="all" />
    <link rel="stylesheet" href="{{url('users/assets/libs/owlcarousel/owl.theme.default.min.css')}}" media="all" />

    <!-- GOOGLE FONT -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,600,600i,700,800,900"/>

    <link rel="stylesheet" href="{{url('users/assets/libs/datepicker/bootstrap-datetimepicker.min.css')}}" media="all"/>

    <!-- MASTER  STYLESHEET  -->
    <link id="lgx-master-style" rel="stylesheet" href="{{url('users/assets/css/style-default.min.css')}}" media="all"/>

    <!-- MODERNIZER CSS  -->
    <script src="{{url('users/assets/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    <style>.float{
        position:fixed;
        width:60px;
        height:60px;
        bottom:40px;
        right:40px;
        background-color:#25d366;
        color:#FFF;
        border-radius:50px;
        text-align:center;
      font-size:30px;
        box-shadow: 2px 2px 3px #999;
      z-index:100;
    }

    .my-float{
        margin-top:16px;
    }

    .float1{
        position:fixed;
        width:60px;
        height:60px;
        bottom:109px;
        right:40px;
        background-color:#25d366;
        color:#FFF;
        border-radius:50px;
        text-align:center;
      font-size:30px;
        box-shadow: 2px 2px 3px #999;
      z-index:100;
    }

    .my-float1{
        margin-top:16px;
    }

    </style>
</head>
<a href="https://api.whatsapp.com/send?phone=917012832207&text=Hai" class="float1" target="_blank">
    <i class="fa fa-whatsapp my-float"></i>
    </a>
    <a href="tel:917012832207" class="float" target="_blank">
        <i class="fa fa-phone my-float1"></i>
        </a>
<body class="home">

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div id="app" class="lgx-container ">
<!-- ***  ADD YOUR SITE CONTENT HERE *** -->
<!--HEADER-->
<header>
    <div id="lgx-header" class="lgx-header">
        <div  style="background-color: white" class="lgx-header-position"> <!--lgx-header-position-fixed lgx-header-position-border lgx-header-position-white lgx-header-fixed-container lgx-header-fixed-container-gap lgx-header-position-white-->
            <div class="lgx-container"> <!--lgx-container-fluid-->
                <nav  class="navbar navbar-default lgx-navbar">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="lgx-logo">
                            <a href="/" class="lgx-scroll">
                                <img src="{{url('users/assets/img/logo.png')}}" alt="Logo"/>
                            </a>
                        </div>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <div class="lgx-nav-right navbar-right">
                            <div class="lgx-cart-area">
                                <a class="lgx-btn lgx-btn-red" href="/apo"><span>Appoinment</span></a>
                            </div>
                        </div>
                        <ul class="nav navbar-nav lgx-nav navbar-right">
                            <li><router-link to="/">Home</router-link></li>
                            {{-- <li><a style="color: rgb(20, 19, 19);" href="/">Home</a></li> --}}
                            <li><router-link to="/about">About</router-link></li>
                            
                            <li><a style="color: rgb(20, 19, 19);" href="/galleryuser">Gallery</a></li>
                            <li><a style="color: rgb(20, 19, 19);" href="/userservice">Departments</a></li>
                            <li><a style="color: rgb(20, 19, 19);" href="/uservideos">Awarnes Videos</a></li>
                            {{-- <li><a style="color: rgb(20, 19, 19);" href="">Events</a></li> --}}
                            <li><a style="color: rgb(20, 19, 19);" href="/doctors">Doctors</a></li>
                            <li><a  style="color: rgb(20, 19, 19);" href="/contact">Contact</a></li>
                   
                        </div><!--/.nav-collapse -->
                </nav>
            </div>
            <!-- //.CONTAINER -->
        </div>
    </div>
</header>
<!--HEADER END-->

        @yield('content')

</div>
<!--FOOTER-->
<footer>
    <div id="lgx-footer" class="lgx-footer lgx-footer-center lgx-footer-black lgx-footer-slider"> <!--lgx-footer-black-->
        <div class="lgx-inner-footer">
            <div class="container">
                <div class="lgx-footer-area">

                    <div class="lgx-footer-single">
                        <h3 class="footer-title">Quick Links</h3>
                        <ul class="list-unstyled lgx-footer-link">
                            <li><a href="#">Gllery</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Video</a></li>
                            <!-- <li><a href="#">Pricing</a></li> -->
                        </ul>
                    </div>
                    <div class="lgx-footer-single">
                        <h3 class="footer-title">Our Services</h3>
                        <ul class="list-unstyled lgx-footer-link">
                            <li><a href="#">Choosing Doctor</a></li>
                            <li><a href="#">proper Treatment</a></li>
                            <li><a href="#">Discuss Doctor</a></li>
                            <li><a href="#">Medical Care</a></li>
                        </ul>
                    </div>
                    <div class="lgx-footer-single">
                        <h3 class="footer-title">Departments</h3>
                        <ul class="list-unstyled lgx-footer-link">
                            <li><a href="#">Dental Surgery</a></li>
                            <li><a href="#">Family Health</a></li>
                            <li><a href="#">Report Bug</a></li>
                            <li><a href="#">Eye Care</a></li>
                        </ul>
                    </div>
                </div>

                <div class="lgx-footer-bottom">
                    <div class="lgx-copyright">
                        <p>Copyright <span>©</span> 2021 All Rights Reserved By <a href="#">Risbuzz</a></p>
                    </div>
                </div>

            </div>
            <!-- //.CONTAINER -->
        </div>
        <!-- //.footer Middle -->
    </div>
</footer>
<!--FOOTER END-->
    {{-- <a class="lgx-backtop show"></a> --}}

</div>
<!--//.LGX SITE CONTAINER-->
<!-- *** ADD YOUR SITE SCRIPT HERE *** -->
<!-- JQUERY  -->

{{-- <script data-cfasync="false" src="../../../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js">
</script> --}}
<script src="{{url('users/assets/js/vendor/jquery-1.12.4.min.js')}}"></script>

<!-- BOOTSTRAP JS  -->
<script src="{{url('users/assets/libs/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{url('users/assets/libs/datepicker/bootstrap-datetimepicker.js')}}"></script>

<!-- Smooth Scroll  -->
<script src="{{url('users/assets/libs/jquery.smooth-scroll.js')}}"></script>

<!-- SKILLS SCRIPT  -->
<script src="{{url('users/assets/libs/jquery.validate.js')}}"></script>

<!-- adding magnific popup js library -->
<script type="text/javascript" src="{{url('users/assets/libs/maginificpopup/jquery.magnific-popup.min.js')}}"></script>

<!-- Owl Carousel  -->
<script src="{{url('users/assets/libs/owlcarousel/owl.carousel.min.js')}}"></script>

<!-- Counter JS -->
<script src="{{url('users/assets/libs/waypoints.min.js')}}"></script>
<script src="{{url('users/assets/libs/counterup/jquery.counterup.min.js')}}"></script>

<!-- SMOTH SCROLL -->
<script src="{{url('users/assets/libs/jquery.smooth-scroll.min.js')}}"></script>
<script src="{{url('users/assets/libs/jquery.easing.min.js')}}"></script>

<!-- type js -->
<script src="{{url('users/assets/libs/typed/typed.min.js')}}"></script>

<!-- header parallax js -->
<script src="{{url('users/assets/libs/header-parallax.js')}}"></script>

<!-- instafeed js -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/instafeed.js/1.4.1/instafeed.min.js"></script>-->
<script src="{{url('users/assets/libs/instafeed.min.js')}}"></script>

<!-- CUSTOM SCRIPT  -->
<script src="{{url('users/assets/js/custom.script.js')}}"></script>
<script src="{{asset('js/app.js')}}" ></script>
{{-- <div class="lgx-switcher-loader"></div> --}}
<!-- For Demo Purpose Only// Remove From Live -->
{{-- <script src="switcher/js/switcherd41d.js?"></script> --}}
<!-- For Demo Purpose Only //Remove From Live-->

</body>


</html>
