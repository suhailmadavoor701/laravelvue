<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Laravel Vue Router Message Example From Scratch - ItSolutionStuff.com</title>
        <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <li><router-link to="/dashboard">Dashboard</router-link></li>
            <li><router-link to="/profile">Profile</router-link></li>
        <h1>Laravel Vue Router Example From Scratch - ItSolutionStuff.com</h1>
        <div id="app">
            <h1>Hello App!</h1>
            <p>
              <!-- use router-link component for navigation. -->
              <!-- specify the link by passing the `to` prop. -->
              <!-- `<router-link>` will be rendered as an `<a>` tag by default -->
              <router-link to="/foo">Go to Foo</router-link>
              <router-link to="/bar">Go to Bar</router-link>
            </p>
            <!-- route outlet -->
            <!-- component matched by the route will render here -->
            <router-view></router-view>
          </div>
        <script src="{{asset('js/app.js')}}" ></script>
    </body>
    
</html>