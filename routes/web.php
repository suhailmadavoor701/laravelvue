<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/{any?}', 'userController@index')->where('any', '.*');

Auth::routes();
Route::get('/galleryuser','userController@galleryuser');
Route::get('/uservideos','userController@uservideos');
Route::get('/apo','userController@apo');
Route::get('/userservice','userController@userservice');

Route::get('/usergallery','userController@gallery');

Route::get('/home', 'HomeController@index')->name('home');
